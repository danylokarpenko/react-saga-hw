import { SET_EDIT_MESSAGE, LOGIN_USER } from './actionTypes';

export const loginUser = ({ email, password }) => ({
    type: LOGIN_USER,
    payload: {
        email,
        password
    }
});

export const setEditMessage = (message) => ({
    type: SET_EDIT_MESSAGE,
    payload: {
        message
    }
});
