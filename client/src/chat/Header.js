import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import '../App.css';

class Header extends Component {
    getUserCount(messages) {
        const users = {};
        let count = 0;
        messages.forEach(({ userId }) => {
            if (!users[userId]) {
                count++;
                users[userId] = true;
            }
        });
        return count;
    }
    getMessageCount(messages) {
        return messages.length;
    }
    getTime(message) {
        const { createdAt } = message;
        const date = new Date(createdAt);
        const minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
        const hours = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
        return `${hours}:${minutes}`;
    }
    getLatestMessageDate(messages) {
        const lastMessage = messages.sort((msg1, msg2) => {
            const firstDate = new Date(msg1.createdAt);
            const secondDate = new Date(msg2.createdAt);
            return firstDate - secondDate;
        })[messages.length - 1];
        const time = this.getTime(lastMessage)
        return time;
    }
    render() {
        const { name, messages } = this.props;
        return (
            <div className="Header">
                <div className="Header-chat-info">
                    <div className="Header-chat-name">
                        <span>{name}</span>
                    </div>
                    <div className="Header-user-count">
                        <span>{this.getUserCount(messages)} participants</span>
                    </div>
                    <div className="Header-message-count">
                        <span>{this.getMessageCount(messages)} messages</span>
                    </div>
                </div>
                <div className="Header-last-message">
                    <span>Last message at {this.getLatestMessageDate(messages)}</span>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        name: state.chat.name,
        messages: state.messages.messages
    }
}

Header.propTypes = {
    name: PropTypes.string.isRequired,
    messages: PropTypes.arrayOf(Object).isRequired
}

export default connect(mapStateToProps)(Header);