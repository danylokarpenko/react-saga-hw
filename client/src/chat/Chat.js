import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Spinner from '../components/Spinner';
import Header from './Header';
import MessageList from '../messages/MessageList';
import MessageInput from '../messages/MessageInput';
import '../App.css';
import { fetchMessages } from '../messages/actions';
import * as actions from './actions';

class Chat extends Component {
    componentDidMount() {
        this.props.fetchMessages();
        if (!this.props.user.id) {
            this.props.history.push('/');
        }
    }
    render() {
        const { loading, history, user } = this.props;
        return (
            loading ? <Spinner /> : (
                <div className="Chat">
                    <Header />
                    <MessageList history={history} />
                    <MessageInput history={history} />
                </div>
            )
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.messages.loading,
        user: state.chat.user
    }
}

const mapDispatchToProps = {
    ...actions,
    fetchMessages
}

Chat.propTypes = {
    loading: PropTypes.bool.isRequired,
    fetchMessages: PropTypes.func.isRequired,
}

Chat.defaultProps = {
    loading: true
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);