import { LOGIN_USER_SUCCESS } from './actionTypes';

const initialState = {
    isLoading: true,
    user: {},
    // {
    //     "id": "9e243930-83c9-11e9-8e0c-8f1a686f4ce4",
    //     "email": "admin@admin",
    //     "password": "123",
    //     "avatar": "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
    //     "role": "admin"
    // },
    name: 'My Chat'
};

export default function (state = initialState, action) {
    switch (action.type) {
        case LOGIN_USER_SUCCESS: {
            const { user } = action.payload;
            return { ...state, user };
        }

        default:
            return state;
    }
}