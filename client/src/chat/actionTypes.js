export const SET_USER = 'SET_USER';
export const SET_EDIT_MESSAGE = 'SET_EDIT_MESSAGE';
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER = "LOGIN_USER";