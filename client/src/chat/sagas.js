import axios from 'axios';
import api from '../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { LOGIN_USER, LOGIN_USER_SUCCESS } from './actionTypes';

export function* loginUser(action) {
	const { email, password } = action.payload;
	try {
        console.log('HERE WE ARE', email, password);
		const user = yield call(axios.post, `${api.url}/auth/login`, { email, password });
		console.log('user', user);
		yield put({ type: LOGIN_USER_SUCCESS, payload: { user: user.data } })
	} catch (error) {
		console.log('LOGIN_USER error:', error.message)
	}
}

function* watchLoginUser() {
	yield takeEvery(LOGIN_USER, loginUser)
}

export default function* usersSagas() {
	yield all([
        watchLoginUser()
	])
};