import React, { useState, useEffect } from 'react'
import { Button, Form } from 'semantic-ui-react'
import { loginUser } from '../chat/actions';
import { connect } from 'react-redux';

const LoginForm = ({
  loginUser,
  history,
  user
}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onSubmit = () => {
    loginUser({ email, password });
  }
  useEffect(() => {
    console.log('Use effect', user);
    if (!user) return;
    const { role } = user;
    if (role === 'user') {
      history.push('/chat');
    } else if (role === 'admin') {
      history.push('/users');
    }
  })
  return (
    <Form onSubmit={onSubmit} >
      <Form.Input onChange={(event) => setEmail(event.target.value)} fluid value={email} type="email" label='Email' placeholder='Email' />
      <Form.Input onChange={(event) => setPassword(event.target.value)} fluid value={password} type="password" label='Password' placeholder='Password' />
      <Button type='submit'>Submit</Button>
    </Form>
  )
}

const mapStateToProps = (state) => {
	return {
    user: state.chat.user
  }
};

const mapDispatchToProps = {
  loginUser
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);