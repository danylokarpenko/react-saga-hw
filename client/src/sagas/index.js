import { all } from 'redux-saga/effects';
import userPageSagas from '../userPage/sagas';
import userSagas from '../users/sagas';
import messages from '../messages/sagas';
import messagePage from '../messagePage/sagas';
import chat from '../chat/sagas';

export default function* rootSaga() {
    yield all([
        userPageSagas(),
        userSagas(),
        messages(),
        chat(),
        messagePage()
    ])
}