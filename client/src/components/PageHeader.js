import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../App.css';
import logo from './logo.svg';
import { connect } from 'react-redux';

class PageHeader extends Component {
    render() {
        const { user } = this.props;
        return (
            <div className="header">
                <div className="flex-row">
                    <img src={logo} className="App-logo" alt="logo" />
                    <span className="logo-title">
                        Relax&Enjoy&Message
                    </span>
                </div>
                <div className="header__userbar">
                    <div className="header__name">{user.user}</div>
                    <div className="header__avatar">
                        <img src={user.avatar} alt="user-avatar" />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.chat.user
    }
}

PageHeader.propTypes = {
    user: PropTypes.object.isRequired
}

export default connect(mapStateToProps)(PageHeader);