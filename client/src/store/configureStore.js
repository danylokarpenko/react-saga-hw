import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../reducers';
import rootSaga from '../sagas/index';

const composeEnhancers =
    typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;


export default function configureStore() {
    const sagaMiddleware = createSagaMiddleware();
    const enhancer = composeEnhancers(
        applyMiddleware(sagaMiddleware),
    );
    const store = createStore(
        rootReducer,
        enhancer
    );

    sagaMiddleware.run(rootSaga);

    return store;
}
