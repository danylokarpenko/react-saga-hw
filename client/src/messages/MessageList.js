import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import moment from 'moment';
import '../App.css';
import TimeLIne from './TimeLine';
import Message from './message';
import * as actions from './actions';
import { connect } from 'react-redux';
import { setEditMessage } from '../chat/actions';

class MessageList extends Component {
    constructor(props) {
        super(props);
        this.messagesEndRef = React.createRef();
    }
    getLastMessage() {
        const { userId } = this.props;
        const messages = this.props.messages.filter(message => message.userId === userId);
        const latestMessage = messages.sort((msg1, msg2) => {
            const firstDate = new Date(msg1.createdAt);
            const secondDate = new Date(msg2.createdAt);
            return secondDate - firstDate;
        })[0];
        return latestMessage
    }
    componentDidMount() {
        document.addEventListener("keydown", this.handleKeyDown);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.handleKeyDown);
    }

    handleKeyDown = (event) => {
        const { key } = event;
        if (key === 'ArrowUp') {
            const message = this.getLastMessage(this.props.userId);
            this.props.setEditMessage(message);
        }
    }
    scrollToBottom = () => {
        this.messagesEndRef.current.scrollIntoView({ behavior: 'smooth' })
    }

    groupMessagesByDays(messages) {
        const groupedResults = _.groupBy(messages, (message) => moment(new Date(message['createdAt'])).startOf('day'));
        return groupedResults;
    }
    getSortedDates(dates) {
        return dates.sort((date1, date2) => {
            const firstDate = new Date(date1);
            const secondDate = new Date(date2);
            return firstDate - secondDate;
        })
    }

    mapMessages(messagesByDate) {
        const dates = Object.keys(messagesByDate);
        const sorted = this.getSortedDates(dates);
        return sorted.map((date) => {
            const messagesForOneDay = messagesByDate[date];
            const messageComponents = messagesForOneDay
                .map(message => <div key={message.id}><Message message={message} history={this.props.history} /></div>);

            return (
                <div key={date} id={date}>
                    <TimeLIne date={date} />
                    {messageComponents}
                </div>
            );
        });
    }
    render() {
        const { messages, userId, history } = this.props;
        const groupedMessages = this.groupMessagesByDays(messages);
        const mapped = this.mapMessages(groupedMessages, userId);
        return (
            <div id="block" className="MessageList card">
                {mapped}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages.messages,
        userId: state.chat.user.id
    }
}

const mapDispatchToProps = {
    ...actions
}

MessageList.propTypes = {
    messages: PropTypes.arrayOf(Object).isRequired,
    userId: PropTypes.string
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);