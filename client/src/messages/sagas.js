import axios from 'axios';
import api from '../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGES, FETCH_MESSAGES_SUCCESS, LOADING } from "./actionTypes";

export function* fetchMessages() {
    yield put({ type: LOADING });
    try {
        const messages = yield call(axios.get, `${api.url}/message`);
        console.log('Fetched Messages', messages);
        yield put({ type: FETCH_MESSAGES_SUCCESS, payload: { messages: messages.data } });
    } catch (error) {
        console.log('Fetched Messages Error:', error);
    }
}

function* watchFetchMessages() {
    yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* addMessage(action) {
	const newMessage = { ...action.payload.data, id: action.payload.id };
    console.log('Try to add message!');
	try {
		yield call(axios.post, `${api.url}/message`, newMessage);
		yield put({ type: FETCH_MESSAGES });
	} catch (error) {
		console.log('createMessage error:', error.message);
	}
}

function* watchAddMessage() {
	yield takeEvery(ADD_MESSAGE, addMessage)
}

export function* updateMessage(action) {
	const id = action.payload.id;
	const updatedMessage = { ...action.payload.data };
	
	try {
		yield call(axios.put, `${api.url}/message/${id}`, updatedMessage);
		yield put({ type: FETCH_MESSAGES });
	} catch (error) {
		console.log('updateMessage error:', error.message);
	}
}

function* watchUpdateMessage() {
	yield takeEvery(UPDATE_MESSAGE, updateMessage)
}

export function* deleteMessage(action) {
	try {
		yield call(axios.delete, `${api.url}/message/${action.payload.id}`);
		yield put({ type: FETCH_MESSAGES })
	} catch (error) {
		console.log('deleteMessage Error:', error.message);
	}
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export default function* messagesSagas() {
	yield all([
        watchFetchMessages(),
        watchAddMessage(),
        watchUpdateMessage(),
        watchDeleteMessage()
	])
};