import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addMessage } from './actions';
import '../App.css';

class MessageInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textInput: ''
        }
    }

    onChange(e, keyword) {
        const value = e.target.value;
        this.setState({
            [keyword]: value
        })
    }
    formMessage(text) {
        const { id, avatar, name: user } = this.props.user;
        return {
            text,
            userId: id,
            avatar,
            user,
            reactions: [],
            createdAt: new Date().toISOString(),
            editedAt: ''
        }
    }

    onClick() {
        if (this.state.textInput.length === 0) return; 
        const message = this.formMessage(this.state.textInput);
        this.props.addMessage(message);
        this.setState({
            textInput: ''
        })
    }

    goToUsers() {
        this.props.history.push('/users');
    }

    render() {
        return (
            <div className="MessageInput">
                <div className="input-group">
                    <textarea placeholder="Type here.." className="form-control" aria-label="With textarea" value={this.state.textInput} onChange={(e) => this.onChange(e, 'textInput')}></textarea>
                    <div className="input-group-prepend">
                        <button className="btn btn-outline-primary" type="button" onClick={() => this.onClick()}>Send</button>
                        {this.props.user.role === 'admin' ? <button onClick={() => this.goToUsers()} className="btn btn-warning usersBtn">Users</button> : null}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.chat.user
    }
}

const mapDispatchToProps = {
    addMessage
}

MessageInput.propTypes = {
    user: PropTypes.object.isRequired,
    addMessage: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);