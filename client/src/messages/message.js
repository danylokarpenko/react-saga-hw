import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { setEditMessage } from '../chat/actions';
import { connect } from 'react-redux';
import * as actions from './actions';

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: null,
            editing: false
        };
    }

    componentDidMount() {
        var block = document.getElementById("block");
        block.scrollTop = block.scrollHeight;
        this.setState();
    }
    getTime(message) {
        const { createdAt } = message;
        const date = new Date(createdAt);
        const minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
        const hours = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
        return `${hours}:${minutes}`;
    }
    setEditMessage() {
        this.props.history.push(`/message/${this.props.message.id}`);
    }
    onLike(message) {
        const { id, reactions } = message;
        const { userId } = this.props;

        const reaction = reactions.find(react => react.userId === userId);

        const updatedMessage = { ...message };
        if (!reaction) {
            updatedMessage.reactions.push({ userId });
        } else {
            updatedMessage.reactions = message.reactions
                .filter(reaction => reaction.userId !== userId);
        }

        this.props.updateMessage(id, updatedMessage);
    }
    render() {
        const { userId, message } = this.props;
        const isMyMessage = userId === message.userId;
        const time = this.getTime(message);
        return (
            isMyMessage ? (
                <div className="position-right">
                    <div className="Message">
                        <span className="message-text">{message.text}</span>
                        <span className="message-time">{time}</span>
                        <svg onClick={this.setEditMessage.bind(this)} width="1em" height="1em" viewBox="0 0 16 16" className="pencil bi bi-pencil" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                            <path fillRule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                        </svg>
                        <svg onClick={() => this.props.deleteMessage(message.id)} width="1em" height="1em" viewBox="0 0 16 16" className="trash bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                            <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                        </svg>
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="heart bi bi-heart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
                        </svg>
                        <span className="message-like">{message.reactions.length}</span>
                    </div>
                </div>
            ) :
                <div className="position-left">
                    <div className="Message">
                        <img src={message.avatar} alt="user-avatar" />
                        <span className="message-text">{message.text}</span>
                        <span className="message-time">{time}</span>
                        <svg onClick={() => this.onLike(message)} width="1em" height="1em" viewBox="0 0 16 16" className="heart bi bi-heart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
                        </svg>
                        <span className="message-like">{message.reactions.length}</span>
                    </div>
                </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userId: state.chat.user.id
    }
}

const mapDispatchToProps = {
    ...actions,
    setEditMessage,
}

Message.protoTypes = {
    message: PropTypes.object.isRequired,
    userId: PropTypes.string.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    updateMessage: PropTypes.func.isRequired,
    setEditMessage: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(Message);