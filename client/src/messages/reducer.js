import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, SET_LIKE, FETCH_MESSAGES, FETCH_MESSAGES_SUCCESS, LOADING } from './actionTypes';

const initialState = {
    messages: [],
    loading: true,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case LOADING: {
            return { ...state, loading: true };
        }

        case FETCH_MESSAGES_SUCCESS: {
            return { ...state, messages: action.payload.messages, loading: false };
        }

        default:
            return state;
    }
}