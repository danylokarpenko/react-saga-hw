import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import Spinner from '../components/Spinner';
import PageHeader from '../components/PageHeader';
import PageFooter from '../components/PageFooter';
import Chat from '../chat/Chat';
import LoginForm from '../LoginForm/LoginForm';
import userPage from '../userPage/index';
import users from '../users/index';
import messagePage from '../messagePage';

const Routing = () => (
  <main className="fill">
    <div>
      <PageHeader />
    </div>
    <Switch>
      <Route exact path="/" component={LoginForm} />
      <Route exact path="/chat" component={Chat} />
      <Route exact path="/user" component={userPage} />
      <Route path="/user/:id" component={userPage} />
      <Route path="/message/:id" component={messagePage} />
      <Route exact path="/users" component={users} />
    </Switch>
    <div>
      <PageFooter />
    </div>
  </main>
);

export default Routing;