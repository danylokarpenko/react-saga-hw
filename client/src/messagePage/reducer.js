import { FETCH_MESSAGE_SUCCESS } from "./actionTypes";

const initialState = {
    messageData: {
        text: '',
        messageId: '',
        id: ''
    }
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_MESSAGE_SUCCESS: {
            const { messageData } = action.payload;
            return {
                ...state,
                messageData
            };
        }

        default:
            return state;
    }
}
