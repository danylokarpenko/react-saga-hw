import { combineReducers } from 'redux';
import chat from '../chat/reducer';
import users from "../users/reducer";
import messages from "../messages/reducer";
import userPage from "../userPage/reducer";
import messagePage from "../messagePage/reducer";

const rootReducer = combineReducers({
    chat,
    users,
    userPage,
    messages,
    messagePage
})

export default rootReducer;