const express = require('express')
const cors = require('cors');
const bodyParser = require('body-parser');

require('./config/db');

const app = express();

app.use(bodyParser.json());

app.use(cors());
app.use(express.json());

app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

app.use('/', express.static('./client/build'));

const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log(`Server has been started http://localhost:${port}`)
});

exports.app = app;