const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {    
    const { body: userData } = req;
    try {
        const user = AuthService.login(userData);
        res.data = user;
    } catch (err) {
        res.status(404);
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;