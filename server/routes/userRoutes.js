const { Router } = require('express');
const UserService = require('../services/userService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router
    .get('/', (req, res, next) => {
        try {
            const users = UserService.findAll();
            res.data = users;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .get('/:id', (req, res, next) => {
        const { id } = req.params;
        try {
            const user = UserService.search({ id });
            console.log('user');
            res.data = user;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .post('/', (req, res, next) => {
        if (res.err) {
            return next();
        }
        const { body: userData } = req;
        try {
            const user = UserService.create(userData);
            res.data = user;
        } catch (err) {
            res.status(400);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .put('/:id', (req, res, next) => {
        if (res.err) return next();

        const { body: userData, params: { id } } = req;
        
        try {
            const updatedUser = UserService.update(id, userData);
            res.data = updatedUser;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .delete('/:id', (req, res, next) => {
        const { id } = req.params;
        try {
            const response = UserService.delete(id);
            res.data = response;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)

module.exports = router;