const userRoutes = require('./userRoutes');
const messageRoutes = require('./messageRoutes');
const authRoutes = require('./authRoutes');

module.exports = (app) => {
    app.use('/api/user', userRoutes);
    app.use('/api/message', messageRoutes);
    app.use('/api/auth', authRoutes);
};