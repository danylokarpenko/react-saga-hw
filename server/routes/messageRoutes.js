const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for message
router
    .get('/', (req, res, next) => {
        try {
            const messages = MessageService.findAll();
            res.data = messages;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .get('/:id', (req, res, next) => {
        const { id } = req.params;
        try {
            const message = MessageService.search({ id });
            res.data = message;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .post('/', (req, res, next) => {
        if (res.err) {
            return next();
        }
        const { body: messageData } = req;
        try {
            const message = MessageService.create(messageData);
            res.data = message;
        } catch (err) {
            res.status(400);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .put('/:id', (req, res, next) => {
        if (res.err) return next();

        const { body: messageData, params: { id } } = req;
        
        try {
            const updatedMessage = MessageService.update(id, messageData);
            res.data = updatedMessage;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .delete('/:id', (req, res, next) => {
        const { id } = req.params;
        try {
            const response = MessageService.delete(id);
            res.data = response;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)

module.exports = router;