const { user } = require('../models/user');

const createUserValid = async (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    for (let prop in req.body) {
        if (!user[prop]) {
            res.status(400);
            res.err = Error('User entity to create is not valid');
            break;
        }
    }
    for (let prop in user) {
        const validator = user[prop];
        const isValid = validator(req.body);
        if (!isValid) {
            res.status(400);
            res.err = Error('User entity to create is not valid');
            break;
        }
    }

    next();
}

const updateUserValid = async (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    for (let prop in req.body) {
        if (!user[prop]) {
            res.status(400);
            res.err = Error('User entity to update is not valid');
            break;
        }
        const validator = user[prop];
        const isValid = validator(req.body);
        if (!isValid) {
            res.status(400);
            res.err = Error('User entity to update is not valid');
            break;
        }
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
