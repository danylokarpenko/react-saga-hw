const responseMiddleware = (req, res, next) => {
    const { err, data } = res;
    if (res.err) {
        const { status = '404', message, statusCode } = res.err;
        res.status(status).json({
            error: true,
            message,
        })
    }
    res.status(200).json(data)
    next();
}

exports.responseMiddleware = responseMiddleware;
