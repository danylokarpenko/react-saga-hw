const { UserRepository } = require('../repositories/userRepository');

class UserService {
    // TODO: Implement methods to work with user
    create(data) {
        const item = UserRepository.create(data);
        if (!item) {
            throw Error('Can\'t create user');
        }
        return item;
    }

    findAll() {
        const items = UserRepository.getAll();
        if(!items) {
            throw Error('Can\'t find users');
        }
        return items;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error('404 User not found');
        }
        return item;
    }
    update(id, data) {
        const item = this.search({ id });
        if (!item) {
            throw Error('404 User not found');
        }
        const updatedItem = UserRepository.update(id, data);        
        if(!updatedItem) {
            throw Error('304 Can\'t update user');
        }
        
        return updatedItem;
    }
    delete(id) {
        const response =  UserRepository.delete(id);
        if(!response) {
            throw Error('Can\'t delete user');
        }
        return response;
    }
}

module.exports = new UserService();