const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
    // TODO: Implement methods to work with message
    create(data) {
        const item = MessageRepository.create(data);
        if (!item) {
            throw Error('Can\'t create message');
        }
        return item;
    }

    findAll() {
        const items = MessageRepository.getAll();
        if(!items) {
            throw Error('Can\'t find messages');
        }
        return items;
    }

    search(search) {
        const item = MessageRepository.getOne(search);
        if(!item) {
            throw Error('404 Message not found');
        }
        return item;
    }
    update(id, data) {
        const item = this.search({ id });
        if (!item) {
            throw Error('404 Message not found');
        }
        const updatedItem = MessageRepository.update(id, data);        
        if(!updatedItem) {
            throw Error('304 Can\'t update message');
        }
        
        return updatedItem;
    }
    delete(id) {
        const response =  MessageRepository.delete(id);
        if(!response) {
            throw Error('Can\'t delete message');
        }
        return response;
    }
}

module.exports = new MessageService();